/*
	ES6 Updates
*/

/*Exponents*/

/*Math.pow() to get the result of a number raised to a given exponent.*/
/*Math.pow(base,exponent)*/

/*let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);//125 = 5 x 5 x 5*/

//Exponent Operators ** = allow us to get the result of a number raised to a given exponent.
let fivePowerOf3 = 5**3;
console.log(fivePowerOf3);//125

/*Mini-Activity*/

/*let fivePowerOf2 = Math.pow(5,2);
let fivePowerOf4 = Math.pow(5,4);
let twoPowerOf2 = Math.pow(2,2);*/

let fivePowerOf2 = 5**2;
let fivePowerOf4 = 5**4;
let twoPowerOf2 = 2**2;

console.log(fivePowerOf2);
console.log(fivePowerOf4);
console.log(twoPowerOf2);

/*Mini-Activity*/

/*function squareRootChecker(number){
	console.log(num**.5);
}

let squareRootOf4 = squareRootChecker(4);
console.log(squareRoot);*/

/*Debug*/

function squareRootChecker(num){
	return num**.5;
}

let squareRootOf4 = squareRootChecker(4);
console.log(squareRootOf4);

/*Template Literals*/
/*
	An ES6 update to creating strings. Represented by backticks (``).

	'',"" - string literals

	`` - backticks/template literals
*/

let sampleString1 = `Charlie`;
let sampleString2 = `Joy`;
console.log(sampleString1);
console.log(sampleString2);
//Charlie and Joy into 1 string?
/*let combinedString = `Charlie` + ` ` + `Joy`;*/
let combinedString = `${sampleString1} ${sampleString2}`;
/*
	We can create a single string to combine our variables. This is with the use of template literals(``) and by embedding JS expressions and variables in the string using ${} or placeholders. 
*/
console.log(combinedString);

/*Template Literals with JS Expressions*/
let num1 = 15;
let num2 = 3;
//What if we want to say a sentence in the console like this:
//"The result of 15 plus 3 is 18"
let sentence = `The result of ${num1} plus ${num2} is ${num1 + num2}`;
console.log(sentence);


/*Mini-Activity*/

let sentence2 = `The result of ${num1} to the power of 2 is ${num1**2}`;
console.log(sentence2);

let sentence3 = `The result of ${num2} times 6 is ${num2*6}`;
console.log(sentence3);

/*Array Destructuring*/
/*When we destructure an array, what we essentially do is to save array items in variables.*/
let array = ["Kobe","Lebron","Jordan"];

/*let goat1 = array[0];
let goat2 = array[1];
let goat3 = array[2];
console.log(goat1);
console.log(goat2);
console.log(goat3);
*/

let [goat1,goat2,goat3] = array;
console.log(goat1);
console.log(goat2);
console.log(goat3);


/*Mini-Activity*/
let array2 = ["Curry","Lillard","Paul","Irving"];

let [pg1,pg2,pg3,pg4] = array2;
console.log(pg1);
console.log(pg2);
console.log(pg3);
console.log(pg4);

/*
	In arrays, the order of items/elements is important and that goes the same to destructuring.
*/

let array3 = ["Jokic","Embiid","Anthony-Towns","Gobert"];
/*
	The order of variables where we want to save our array items is also important.

	How can we skip an item?
	You can skip an item by adding another separator (,) and by not providing a variable to save the item we want to skip.
*/
let [center1,,center3] = array3;
console.log(center1);
console.log(center3);

/*Mini-Activity*/

let array4 = ["Draco Malfoy","Hermione Granger","Harry Potter","Ron Weasley","Professor Snape"];

let [,gryffindor1, gryffindor2, gryffindor3] = array4;
console.log(gryffindor1);
console.log(gryffindor2);
console.log(gryffindor3);

/*
	Destructuring will allow us to destructure an array and save items in variables and also into constants.

	let/const [variable1,variable2] = array;
*/

const [slytherin1,,,,slytherin2] = array4;
console.log(slytherin1);
console.log(slytherin2);

//variables can be updated.
gryffindor1 = "Emma Watson";
console.log(gryffindor1);

//constants cannot be updated.
/*slytherin1 = "Tom Felton";
console.log(slytherin1);*/

/*Object Destructuring*/
/*It will allow us to destructure an object by saving/adding the values of an object's property into respective variables.*/

let pokemon = {
	name: "Blastoise",
	level: 40,
	health: 80
}

/*Mini-Activity*/

let sentence4 = `The pokemon's name is ${pokemon.name}.`;
let sentence5 = `It is a level ${pokemon.level} pokemon.`;
let sentence6 = `It has at least ${pokemon.health} health points.`;

console.log(sentence4);
console.log(sentence5);
console.log(sentence6);

//We can save the values of an object's properties into variables.
//By Object Destructuring:
/*
	Array Destructuring, order was important and the name of the variable we save our items in is not important.

	Object destructuring, order is not important, however, the name of the variables should match the properties of the object.
*/

let {health,name,level,trainer} = pokemon;
console.log(health);
console.log(name);
console.log(level);
console.log(trainer);//undefined because there is no trainer property in our pokemon object.

/*Mini Activity*/

let person = {
	name: "Paul Phoenix",
	age: 31,
	birthday: "January 12, 1991"
}

function greet({name,age,birthday}){
	//destructure your object in the function
	// let {name,age,birthday} = object;
	console.log(`Hi! My name is ${name}.`);
	console.log(`I am ${age} years old.`);
	console.log(`My birthday is on ${birthday}.`);
}

greet(person);

/*Mini-Activity*/

let  actors = ["Tom Hanks","Leonardo Dicaprio","Anthony Hopkins","Ryan Reynolds"];
let director = {
	name: "Alfred Hitchcock",
	birthday: "August 13, 1889",
	isActive: false,
	movies: ["The Birds","Psycho","North by Northwest"]
};

/*let {actor1,actor2,actor3} = actor;

function displayDirector(person){
	let {name,birthday,movie,nationality} = person;

	console.log(`The Director's name is $name`);
	console.log(`He was born on ${person.birthday}`);
	console.log(`His Movies include:`);
	console.log(movies);
}

console.log(actor1);
console.log(actor2);
console.log(actor4);

displayDirector(director);*/

let [actor1,actor2,actor3] = actors;

function displayDirector({name,birthday,movies}){
	// let {name,birthday,movies} = person;

	console.log(`The Director's name is ${name}.`);
	console.log(`He was born on ${birthday}.`);
	console.log(`His Movies include:`);
	console.log(`${movies}`);
}

console.log(actor1);
console.log(actor2);
console.log(actor3);

displayDirector(director);

/*Arrow Functions*/

	//Arrow Functions are an alternative way of writing/declaring functions. However, there are significant differences between our regular/traditional functions and arrow functions.

	//regular/traditional function
	function displayMsg(){
		console.log(`Hello, World!`)
	}

	displayMsg();

	//arrow functions
	const hello = () => {
		console.log(`Hello from Arrow!`)
	}

	hello();

	/*Mini-Activity*/

	/*function greet(personParams){
		console.log(`Hi, ${personParams.name}!`);
	}*/

	const greetPerson = (personParams) => {
		console.log(`Hi, ${personParams.name}!`);
	}

	greetPerson(person);

	//anonymous functions in array methods

	//It allows us to iterate/loop over all items in an array.
	//An anonymous function is added so we can perform tasks per item in the array.
	//This anonymous function receives the current item being iterated/looped
	
	/*actors.forEach(function(actor){
		console.log(actor);
	})*/

	//Note: traditional function cannot work without the curly braces.
	//Note: However, arrow functions can BUT it has to be a one-liner.
	actors.forEach(actor=>console.log(actor));

	///.map() is similar to forEach wherein we can iterate over all items in our array.
	//The difference is we can return items from a map and create a new array out of it.

	let numberArr = [1,2,3,4,5];

	/*let multipliedby5 = numberArr.map(function(number){
		return number*5
	})*/

	//arrow functions do not need the return keyword to return a value. This is called implicit return.
	//When arrow functions have a curly brace, it will need to have a return keyword to return a value.

	let multipliedby5 = numberArr.map(number => number*5);

	console.log(multipliedby5);

	/*Implicit Return for Arrow Functions*/

	/*function addNum(num1,num2){
		return num1+num2
	}

	let sum1 = addNum(5,10);
	console.log(sum1);*/

	const subtractNum = (num1,num2) => num1 - num2;
	let difference1 = subtractNum(45,15);
	console.log(difference1);
	//Even without a return keyword, arrow functions can return a value. So long as its code block is not wrapped with {}.


	/*Mini-Activity*/
	const addNum = (num1,num2) => num1 + num2;
	let sumExample = addNum(50,10);
	console.log(sumExample);

	/*this keyword in a method*/

	let protagonist = {
		name: "Cloud Strife",
		occupation: "SOLDIER",
		greet: function(){
			console.log(this);
			//this refers to the object where the method is in for methods created as traditional functions.
			console.log(`Hi! I am ${this.name}.`);
		},
		introduceJob: () => {
			//this keyword in an arrow function does not refer to parent object, instead it refers to the global window object or the whole page.
			console.log(this)
			console.log(`I work as a ${this.occupation}`)
		}
	}

	protagonist.greet();//this refers to parent object.
	protagonist.introduceJob();//this.occupation results to undefined because this in an arrow function refers to the global window object.

	//Class-Based Objects Blueprints
		//In Javascript, Classes are templates/blueprints creating objects.
		//We can create objects out of the use of Classes.
		//Before the introduction of Classes in JS, we mimic this behavior to create objects out of templates with the use of constructor functions.

		//Constructor function

			function Pokemon(name,type,level){
				this.name = name;
				this.type = type;
				this.level = level;
			}

			let pokemon1 = new Pokemon("Sandshrew","Ground,15");
			console.log(pokemon1);

		//ES6 Class Creation
			//We have a more distinctive way of creating classes instead of just our constructor functions

			class Car {
				constructor(brand,name,year){
					this.brand = brand;
					this.name = name;
					this.year = year;
				}
			}

			let car1 = new Car("Toyota","Vios","2022");
			console.log(car1);